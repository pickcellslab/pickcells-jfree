package org.pickcellslab.jfree.simple;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter2D;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter2DFactory;

@CoreImpl
public class JFSimpleScatterFactory implements SimpleScatter2DFactory {

	@Override
	public SimpleScatter2D createChart() {
		return new JFSimpleScatter();
	}

}
