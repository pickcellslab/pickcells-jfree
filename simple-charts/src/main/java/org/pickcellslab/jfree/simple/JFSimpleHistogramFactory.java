package org.pickcellslab.jfree.simple;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogram;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;

@CoreImpl
public class JFSimpleHistogramFactory implements SimpleHistogramFactory {

	@Override
	public SimpleHistogram createChart() {
		return new JFSimpleHistogram();
	}


}
