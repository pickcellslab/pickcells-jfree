package org.pickcellslab.jfree.simple;

import java.awt.Color;
import java.awt.Component;
import java.text.Format;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.xy.AbstractIntervalXYDataset;
import org.pickcellslab.foundationj.services.simplecharts.DefaultSimpleChartModel;
import org.pickcellslab.foundationj.services.simplecharts.DisplayedRangeListener;
import org.pickcellslab.foundationj.services.simplecharts.SeriesChangeListener;
import org.pickcellslab.foundationj.services.simplecharts.SimpleChartModel;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogram;
import org.pickcellslab.foundationj.services.simplecharts.SimpleSeries;

public class JFSimpleHistogram implements SimpleHistogram, SeriesChangeListener<double[]> {

	private final DefaultSimpleChartModel<double[]> model;
	private final JFreeChart chart;
	private final ChartPanel chartPanel;
	private final SimpleHistogramDataset dataset;

	private final List<DisplayedRangeListener> rlstrs = new ArrayList<>();
	
	
	

	public JFSimpleHistogram() {

		this.model = new DefaultSimpleChartModel<>();
		model.addSeriesChangeListener(this);


		dataset = new SimpleHistogramDataset();


		chart = 
				ChartFactory.createHistogram(
						"",
						"No dataset loaded yet",
						"Counts",
						dataset,
						PlotOrientation.VERTICAL,
						true,
						true,
						true);

		chart.getXYPlot().setForegroundAlpha(0.40f);
		chart.getXYPlot().setBackgroundPaint(Color.WHITE);

		final XYPlot plot = chart.getXYPlot();
		XYBarRenderer r = ((XYBarRenderer) plot.getRenderer());
		r.setShadowVisible(false);
		r.setBarPainter(new StandardXYBarPainter());
		
		
		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseZoomable(true);
		chartPanel.setBackground(Color.WHITE);
		chartPanel.setPreferredSize(new java.awt.Dimension(250, 250));

		chart.getXYPlot().getDomainAxis().addChangeListener(l->rlstrs.forEach(o->o.rangeChanged(this)));

	}


	@Override
	public void setTitle(String title) {
		chart.setTitle(title);
	}

	@Override
	public Component getScene() {
		return chartPanel;
	}

	@Override
	public SimpleChartModel<double[]> getModel() {
		return model;
	}



	@Override
	public void setAxisLabel(int axis, String label) {
		if(axis == 0)
			chart.getXYPlot().getDomainAxis().setLabel(label);
		else
			chart.getXYPlot().getRangeAxis().setLabel(label);
	}


	@Override
	public void showLegend(boolean b) {
		chart.getLegend().setVisible(b);
	}

	


	@Override
	public void setAxisFormat(int axis, Format format) {
		NumberAxis na = null;
		if(axis == 0){
			na = (NumberAxis) chart.getXYPlot().getDomainAxis();
		}
		else
			na = (NumberAxis) chart.getXYPlot().getRangeAxis();
		na.setNumberFormatOverride((NumberFormat) format);		
	}



	@Override
	public void seriesChanged(SeriesChange c, SimpleSeries<double[]> s) {
		if(c == SeriesChange.ADDED){
			dataset.addSeries(s);
		}
		else{
			dataset.removeSeries(s);
		}
	}

	
	
	@Override
	public void setSeriesColor(int series, Color color) {
		chart.getXYPlot().getRenderer().setSeriesPaint(series, color);
	}
	
	
	

	@Override
	public void setDisplayedAxisRange(int i, double lower, double upper) {
		chart.getXYPlot().getDomainAxis().setRange(lower, upper);
	}





	@Override
	public double getDisplayedMinimumValue() {
		return chart.getXYPlot().getDomainAxis().getLowerBound();
	}


	@Override
	public double getDisplayedMaximumValue() {
		return chart.getXYPlot().getDomainAxis().getUpperBound();
	}


	
	@Override
	public void addDisplayedRangeListener(DisplayedRangeListener l) {
		rlstrs.add(l);
	}


	@Override
	public void removeDisplayedRangeListener(DisplayedRangeListener l) {
		rlstrs.remove(l);
	}

	



	@SuppressWarnings("serial")
	private class SimpleHistogramDataset extends AbstractIntervalXYDataset{


		List<SimpleSeries<double[]>> series = new ArrayList<>();

		public void addSeries(SimpleSeries<double[]> s) {
			series.add(s);
			this.fireDatasetChanged();
		}


		public void removeSeries(SimpleSeries<double[]> s) {
			System.out.println(s + " removed! (JFSimpleChart)");
			if(series.remove(s))
				this.fireDatasetChanged();
		}




		@Override
		public Number getStartX(int sr, int item) {			
			SimpleSeries<double[]> s = series.get(sr);
			return s.getValue(item)[0];
		}



		@Override
		public Number getEndX(int sr, int item) {
			SimpleSeries<double[]> s = series.get(sr);
			return s.getValue(item)[1];
		}

		@Override
		public Number getStartY(int sr, int item) {
			return 0;
		}

		@Override
		public Number getEndY(int sr, int item) {
			SimpleSeries<double[]> s = series.get(sr);
			double d = 0;
			for(int i = 0; i<s.numValues(); i++){
				d = Math.max(d, s.getValue(i)[2]);
			}
			return d;
		}

		@Override
		public int getItemCount(int sr) {
			return series.get(sr).numValues();
		}

		@Override
		public Number getX(int sr, int i) {
			return series.get(sr).getValue(i)[0];
		}

		@Override
		public Number getY(int sr, int i) {
			return series.get(sr).getValue(i)[2];
		}

		@Override
		public int getSeriesCount() {
			return series.size();
		}

		@Override
		public Comparable<?> getSeriesKey(int s) {
			return series.get(s).id();
		}

	}



}
