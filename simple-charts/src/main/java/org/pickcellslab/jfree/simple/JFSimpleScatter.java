package org.pickcellslab.jfree.simple;

import java.awt.Color;
import java.awt.Component;
import java.text.Format;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.data.xy.AbstractXYDataset;
import org.pickcellslab.foundationj.services.simplecharts.DefaultSimpleChartModel;
import org.pickcellslab.foundationj.services.simplecharts.SeriesChangeListener;
import org.pickcellslab.foundationj.services.simplecharts.SimpleChartModel;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter2D;
import org.pickcellslab.foundationj.services.simplecharts.SimpleSeries;

public class JFSimpleScatter implements SimpleScatter2D, SeriesChangeListener<double[]>{


	private final DefaultSimpleChartModel<double[]> model;
	private final JFreeChart chart;
	private final ChartPanel chartPanel;
	private final SimpleSeriesCollection dataset;




	public JFSimpleScatter() {

		this.model = new DefaultSimpleChartModel<>();
		model.addSeriesChangeListener(this);

		dataset = new SimpleSeriesCollection();		

		//Initialise the JFreeChart
		chart = ChartFactory.createScatterPlot("2D Scatter Plot",//.createScatterPlot("2D Scatter Plot",
				"X",
				"Y",
				dataset,
				PlotOrientation.VERTICAL,
				false,true,true
				);

		chart.getXYPlot().setForegroundAlpha(0.6f);

		chart.getXYPlot().setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);
		
		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseZoomable(true);
		chartPanel.setBackground(Color.WHITE);
		chartPanel.setPreferredSize(new java.awt.Dimension(450, 450));



	}


	@Override
	public void seriesChanged(SeriesChange c, SimpleSeries<double[]> s) {
		if(c == SeriesChange.ADDED){
			dataset.addSeries(s);
		}
		else{
			dataset.removeSeries(s);
		}
	}




	@Override
	public void setTitle(String title) {
		chart.setTitle(title);
	}

	@Override
	public Component getScene() {
		return chartPanel;
	}

	@Override
	public SimpleChartModel<double[]> getModel() {
		return model;
	}

	
	
	

	@Override
	public void setAxisLabel(int axis, String label) {
		if(axis == 0)
			chart.getXYPlot().getDomainAxis().setLabel(label);
		else
			chart.getXYPlot().getRangeAxis().setLabel(label);
	}


	@Override
	public void showLegend(boolean b) {
		chart.getLegend().setVisible(b);
	}

	


	@Override
	public void setAxisFormat(int axis, Format format) {
		NumberAxis na = null;
		if(axis == 0){
			na = (NumberAxis) chart.getXYPlot().getDomainAxis();
		}
		else
			na = (NumberAxis) chart.getXYPlot().getRangeAxis();
		na.setNumberFormatOverride((NumberFormat) format);		
	}
	
	
	
	
	@Override
	public void setSeriesColor(int series, Color color) {
		chart.getXYPlot().getRenderer().setSeriesPaint(series, color);
	}
	
	


	private class SimpleSeriesCollection extends AbstractXYDataset{


		private final List<SimpleSeries<double[]>> list = new ArrayList<>();

		public void addSeries(SimpleSeries<double[]> series){
			list.add(series);
			this.fireDatasetChanged();
		}

		public void removeSeries(SimpleSeries<double[]> series){
			if(list.remove(series)){
				this.fireDatasetChanged();
				System.out.println("JFSimpleScatter : series removed " + series.id());	
			}
		}


		@Override
		public int getItemCount(int series) {
			return list.get(series).numValues();
		}

		@Override
		public Number getX(int series, int item) {
			return list.get(series).getValue(item)[0];
		}

		@Override
		public Number getY(int series, int item) {
			return list.get(series).getValue(item)[1];
		}

		@Override
		public int getSeriesCount() {
			return list.size();
		}

		@Override
		public Comparable getSeriesKey(int series) {
			return list.get(series).id();
		}


	}





}
