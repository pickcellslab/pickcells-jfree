package org.pickcellslab.jfree.dbcharts;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public class XYDataSetAction implements Action<WritableDataItem, Void> {


	private final XYSeriesCollection dataset;
	private final Function<WritableDataItem, ? extends Number> xF;
	private final Function<WritableDataItem, ? extends Number> yF;

	private final Map<Object, XYSeries> coords = new HashMap<>();


	public XYDataSetAction(XYSeriesCollection dataset, Function<WritableDataItem, ? extends Number> f1, Function<WritableDataItem, ? extends Number> f2) {
		Objects.requireNonNull(dataset, "dataset is null");
		Objects.requireNonNull(f1, "f1 is null");
		Objects.requireNonNull(f2, "f2 is null");

		this.dataset = dataset;
		this.xF = f1;
		this.yF = f2;
	}



	@Override
	public ActionMechanism<WritableDataItem> createCoordinate(Object key) {
		XYSeries series = new XYSeries(key.toString());
		System.out.println("XYDataSetAcion : Series created "+key.toString());
		coords.put(key.toString(), series);
		return new XYSeriesMechanism(series);
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {
		dataset.addSeries(coords.get(key.toString()));			
		return null;
	}

	@Override
	public String description() {
		return "Populate 2D XY Plot";
	}



	class XYSeriesMechanism implements ActionMechanism<WritableDataItem>{

		private XYSeries series;

		public XYSeriesMechanism(XYSeries series) {
			this.series = series;
		}

		@Override
		public void performAction(WritableDataItem i) throws DataAccessException {
			Number x = xF.apply(i);
			if(null!=x){
				Number y = yF.apply(i);
				series.add(new DBXYDataItem(x,y, i.getAttribute(WritableDataItem.idKey).get()), false);
			}
		}

	}




}
