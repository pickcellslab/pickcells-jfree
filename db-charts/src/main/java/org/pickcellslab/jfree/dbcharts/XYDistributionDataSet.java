package org.pickcellslab.jfree.dbcharts;

import java.util.Objects;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.jfree.chart.renderer.PaintScale;
import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYZDataset;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.pickcells.api.app.charts.RGBLuts;

public class XYDistributionDataSet implements XYZDataset {
   
	private final Distribution<double[]> distribution;
	private final double minX;
	private final double maxX;
	private final double minY;
	private final double maxY;
		
	private final double xOffset;
	private final double yOffset;
	private double x;
	
	SummaryStatistics stats = new SummaryStatistics();

	public XYDistributionDataSet(Distribution<double[]> distri,  double minX, double maxX, double minY, double maxY) {
		Objects.requireNonNull(distri);
		this.distribution = distri;
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		
		xOffset = (maxX-minX)/250f;
		yOffset = (maxY-minY)/250f;
		
		System.out.println("X offset = "+xOffset);
		System.out.println("Y offset = "+yOffset);
	}
	
	
	public Distribution<double[]> getDistribution(){
		return distribution;
	}
	
	public int getSeriesCount() {
        return 1;
    }
    
	public int getItemCount(int series) {
        return 62500;
    }
    
	public Number getX(int series, int item) {
        return new Double(getXValue(series, item));
    }
    
	public double getXValue(int series, int item) {
		x = item % (250d) * xOffset + minX;
		//System.out.println("X = "+x);
        return x;
    }
    
	public Number getY(int series, int item) {
        return new Double(getYValue(series, item));
    }
    
	public double getYValue(int series, int item) {
        return Math.floor(item / 250d) * yOffset  + minY;
    }
    
	public Number getZ(int series, int item) {
        return new Double(getZValue(series, item));
    }
    
    public double getZValue(int series, int item) {
        double x = getXValue(series, item);
        double y = getYValue(series, item);
        double d = distribution.density(new double[]{x,y});
        stats.addValue(d);
        return d;
    }
    
    public void addChangeListener(DatasetChangeListener listener) {
        // ignore - this dataset never changes
    }
    
    public void removeChangeListener(DatasetChangeListener listener) {
        // ignore
    }
    
    public DatasetGroup getGroup() {
        return null;
    }
    
    public void setGroup(DatasetGroup group) {
        // ignore
    }
    
    public Comparable getSeriesKey(int series) {
        return distribution.summary();
    }
    
    public int indexOf(Comparable seriesKey) {
        return 0;
    }
    
    public DomainOrder getDomainOrder() {
        return DomainOrder.ASCENDING;
    }        
    
    
    public PaintScale createStretchedScale(){
    	double max = stats.getMax() * 40;    	
    	stats.clear();
    	return new LUTScale(0,max,RGBLuts.SPECTRUM);
    	//return new GrayPaintScale(0, 0.001);
    }
    
    public void print(){
    	System.out.println(stats);
    }
}; 
