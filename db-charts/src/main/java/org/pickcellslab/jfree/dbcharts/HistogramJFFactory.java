package org.pickcellslab.jfree.dbcharts;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.fitting.UniVariateRealDistributionFitterFactory;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;


@Module
public class HistogramJFFactory implements DBViewFactory{

	private final UITheme theme;
	private final NotificationFactory notif;
	private final List<UniVariateRealDistributionFitterFactory> fitters;
	
	
	public HistogramJFFactory(UITheme theme, NotificationFactory notif, List<UniVariateRealDistributionFitterFactory> fitters) {
		this.theme = theme;
		this.fitters = fitters;
		this.notif = notif;
	}
	
	

	@Override
	public String toString() {
		return "Histogram";
	}




	@Override
	public String description() {
		return "Create an Histogram using The JFreeChart Library";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}







	@Override
	public String name() {
		return "Histogram";
	}




	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) {

		//Create the scatter
		HistogramJF histo = new HistogramJF(helper, notif, fitters);

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> QueryConfigs.newTargetedOnlyConfig("").setSynchronizeTarget(true).build(), histo.getModel())
				.addChangeDimensionsButton(histo)
				.addCameleonControl(histo, null)
				.addDistributionFittingControl(histo.getModel())
				.addExportDataButton(histo)
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar,BorderLayout.NORTH);
		scene.add(new JScrollPane(histo.getView()),BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());


	}




	@Override
	public Icon icon() {
		return theme.icon(IconID.Charts.HISTOGRAM, 24);
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}



}
