package org.pickcellslab.jfree.dbcharts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer.FillType;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.HistogramType;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.fitting.MixtureModel;
import org.pickcellslab.foundationj.dataviews.fitting.UniVariateRealDistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.ScalarDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.NumericChartModel;
import org.pickcellslab.pickcells.api.app.charts.StandardNumericDBChart;
import org.pickcellslab.pickcells.api.app.picking.AbstractTotiPicker;

public class HistogramJF implements StandardNumericDBChart<Double, SeriesAppearance>{

	
	private final NotificationFactory notif;
	
	private NumericChartModel<DataItem,Double> model;

	private String currentQuery;


	private final JFreeChart histoChart;	
	private final JFreeChart boxChart;

	private final ChartPanel histoPanel;
	private final ChartPanel boxPanel;
	
	
	private final JPanel view;
	
	private PublicHistogramDataSet histoDataset = new PublicHistogramDataSet();	
	private DefaultBoxAndWhiskerCategoryDataset boxDataset = new DefaultBoxAndWhiskerCategoryDataset();
	
	private XYSeriesCollection fits = new XYSeriesCollection();

	

	private Dimension<DataItem, Number> dim;

	



	public HistogramJF(DBViewHelp helper, NotificationFactory notif, List<UniVariateRealDistributionFitterFactory> fitters) {

		this.notif = notif;
		
		//Create the default model
		final Predicate<Integer> p = i -> i==1;
		model = new NumericChartModel<>(p, (List)fitters);

		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);

		
		
		

		//Top panel is a box plot representation of the distribution
		boxChart = ChartFactory.createBoxAndWhiskerChart(
				"   ",
				"",
				"",
				boxDataset,
				false);

		boxChart.getCategoryPlot().setOrientation(PlotOrientation.HORIZONTAL);
		
		boxChart.getCategoryPlot().getRangeAxis().setVisible(false);
		boxChart.getCategoryPlot().getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI/2));
		
	//	boxChart.getCategoryPlot().getDomainAxis().setLabelFont();
		boxChart.getCategoryPlot().setBackgroundPaint(Color.WHITE);
		boxChart.getCategoryPlot().getDomainAxis().setTickLabelsVisible(false);
		
		//FIXME This is a silly fix to shift the align the 2 plots, there must be something more appropriate in the library
		this.boxChart.getCategoryPlot().getDomainAxis().setLabelFont(new Font("Verdana", Font.BOLD, 30));
		this.boxChart.getCategoryPlot().getDomainAxis().setLabelPaint(Color.WHITE);
		this.boxChart.getCategoryPlot().getDomainAxis().setLabel("Series");
		
		//BoxAndWhiskerRenderer r = (BoxAndWhiskerRenderer) this.boxChart.getXYPlot().getRenderer();
		
		
		boxPanel = new ChartPanel(boxChart);
		boxPanel.setBorder(BorderFactory.createEmptyBorder(10, 0,0,0));
		boxPanel.setMouseZoomable(false);
		boxPanel.setPreferredSize(new java.awt.Dimension(250,50));
		
		boxPanel.setBackground(Color.BLACK);
		boxChart.setBackgroundPaint(Color.BLACK);		
		boxChart.getTitle().setPaint(Color.WHITE);
		boxChart.getCategoryPlot().getDomainAxis().setTickLabelPaint(Color.WHITE);
		boxChart.getCategoryPlot().getRangeAxis().setTickLabelPaint(Color.WHITE);
		boxChart.getCategoryPlot().getDomainAxis().setLabelPaint(Color.WHITE);
		boxChart.getCategoryPlot().getRangeAxis().setLabelPaint(Color.WHITE);
		
		boxChart.getCategoryPlot().setBackgroundPaint(Color.BLACK); 
		boxChart.getCategoryPlot().setDomainGridlinePaint(Color.GRAY); 
		
		
	//	DistriPicker boxPicker = new DistriPicker();
	//	boxPanel.addChartMouseListener(boxPicker);		
	//	DBViewHelp.registerSeriesConsumersFor(boxPicker);
		
		
		

		//Create the histogram panel
		histoChart = 
				ChartFactory.createHistogram(
						"",
						"No dataset loaded yet",
						"Counts",
						histoDataset,
						PlotOrientation.VERTICAL,
						true,
						true,
						true);

		histoChart.getXYPlot().setForegroundAlpha(0.40f);
		XYSplineRenderer r = new XYSplineRenderer();
		r.setFillType(FillType.TO_LOWER_BOUND);
		r.setBaseShapesVisible(false);
		r.setPrecision(2);
		histoChart.getXYPlot().setRenderer(r);
		//boxChart.getCategoryPlot().setRangeAxis(histoChart.getXYPlot().getRangeAxis());
		
		
		
		histoPanel = new ChartPanel(histoChart);
		histoPanel.setMouseZoomable(true);
		
		histoPanel.setBackground(Color.BLACK);
		histoPanel.getChart().setBackgroundPaint(Color.BLACK);		
		histoPanel.getChart().getTitle().setPaint(Color.WHITE);
		histoPanel.getChart().getXYPlot().getDomainAxis().setTickLabelPaint(Color.WHITE);
		histoPanel.getChart().getXYPlot().getRangeAxis().setTickLabelPaint(Color.WHITE);
		histoPanel.getChart().getXYPlot().getDomainAxis().setLabelPaint(Color.WHITE);
		histoPanel.getChart().getXYPlot().getRangeAxis().setLabelPaint(Color.WHITE);
		
		histoPanel.getChart().getXYPlot().setBackgroundPaint(Color.BLACK); 
		histoPanel.getChart().getXYPlot().setDomainGridlinePaint(Color.GRAY);
		
		histoPanel.getChart().getLegend().setBackgroundPaint(Color.BLACK);
		histoPanel.getChart().getLegend().setItemPaint(Color.WHITE);
		
		histoPanel.setPreferredSize(new java.awt.Dimension(250, 200));
		
		DistriPicker histoPicker = new DistriPicker();
		histoPanel.addChartMouseListener(histoPicker);
		
		helper.registerDistributionConsumerFor(histoPicker);
		
		
		boxChart.getCategoryPlot().setFixedDomainAxisSpace(histoChart.getXYPlot().getFixedRangeAxisSpace());
		//boxChart.getCategoryPlot().setRangeAxis(histoChart.getXYPlot().getRangeAxis());
		
		
	//	boxChart.getCategoryPlot().setFixedRangeAxisSpace(histoChart.getXYPlot().getFixedRangeAxisSpace());
		
		
		

		view = new JPanel(new BorderLayout());
		view.add(boxPanel,BorderLayout.NORTH);
		view.add(histoPanel,BorderLayout.CENTER);

		
		
	}





	@Override
	public void setModel(NumericChartModel<DataItem,Double> model) {
		if(model!=null){
			model.removeDataEvtListener(this);			
		}		
		this.model = model;
		model.addBrokerChangedListener(this);		
	}



	@Override
	public NumericChartModel<DataItem,Double> getModel() {
		return model;
	}





	@Override
	public String getQueryUnderFocus() {
		return currentQuery;
	}





	//--------------------------------------------------------------------------------------------------------------
	// Series appearance and visibility

	@Override
	public void setSeriesVisible(String query, int series, boolean visible) {
		int s = model.getDataSet().firstSeriesIndex(query)+series;
		histoChart.getXYPlot().getRenderer(0).setSeriesVisible(s, visible);
	}




	@Override
	public SeriesAppearance getSeriesAppearance(String query, int series) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setSeriesAppearance(String query, int series, SeriesAppearance appearance) {
		// TODO Auto-generated method stub

	}








	//-----------------------------------------------------------------------------------------------------------------
	// Fitting




	@Override
	public void fittingUpdated(FittingEvent evt) {

		
		
		//Make sure the graph is properly configured to display the fits
		if(histoChart.getXYPlot().getRenderer(1) == null){
			XYSplineRenderer r = new XYSplineRenderer();
			r.setBaseShapesVisible(false);
		//	r.setBaseOutlineStroke(new Stroke());
			histoChart.getXYPlot().setRenderer(1,r);
		}

		
		FitResult<? extends Distribution<Double>> fr = model.dimensionBroker(this.currentQuery).getDistributionFit(evt.stringId());

		Distribution<Double> d = fr.getFit();


		//Remove series to be replaced
		String prefix = fr.getId();
		int c = 0;
		for ( int s = 0; s<fits.getSeriesCount(); s++){
			if(fits.getSeriesKey(s).toString().contains(prefix)){
				fits.removeSeries(s-(c++));
				System.out.println("HistogramJF : removed -> "+s);
				System.out.println("HistogramJF : count -> "+c);
			}
			else
				System.out.println("Fit not removed : "+fits.getSeriesKey(s));
		}




		//Create sampling:
		double min = histoChart.getXYPlot().getDomainAxis().getRange().getLowerBound();
		double max = histoChart.getXYPlot().getDomainAxis().getRange().getUpperBound();

		double sampling = (max-min)/ 200;


		//Get the series index for the first series of the currentQuery
		int index = model.getDataSet().firstSeriesIndex(currentQuery);

		//Total number of objects in the given query
		double numItems = histoDataset.getTotal(index)*histoDataset.getBinWidth(index);

		System.out.println("Number of items "+numItems+" in "+currentQuery);

		if(MixtureModel.class.isAssignableFrom(d.getClass())){			

			@SuppressWarnings("unchecked")
			MixtureModel<Double,?> mm = (MixtureModel<Double, ?>) d;

			Distribution<Double>[] distris = mm.distributions();

			if(distris.length>1){

				double[] weights = mm.weights();

				for(int j = 0; j<mm.numDistributions(); j++){	

					XYSeries series = new XYSeries(prefix+" "+j, false);				
					for(double i = min; i<max; i+=sampling){
						series.add(i, weights[j] * distris[j].density(i) * numItems);
					}		
					fits.addSeries (series);
					int f = j;
					histoChart.getXYPlot().getRenderer(1).setSeriesToolTipGenerator(fits.getSeriesCount()-1,(ds,a,b)-> distris[f].summary() );
				}
			}

		}


		// Display the full distribution		
		XYSeries series = new XYSeries(prefix, false);

		for(double i = min; i<max; i+=sampling){
			series.add(i,d.density(i) * numItems);
		}

		fits.addSeries (series);
		histoChart.getXYPlot().getRenderer(1).setSeriesToolTipGenerator(fits.getSeriesCount()-1,(ds,a,b)-> fr.getDescription() );

		histoChart.getXYPlot().setDataset(1, fits);
		histoChart.getXYPlot().setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		
		
	}



	@Override
	public SeriesAppearance getFitAppearance(FitResult<?> fit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFitAppearance(FitResult<?> fit, SeriesAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFitVisible(FitResult<?> fit, boolean isVisible) {
		// TODO Auto-generated method stub

	}







	//--------------------------------------------------------------------------------------------------------------------------------
	// Listen for changes to the dataset

	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {		
		
		//update the boxplot
		boxDataset = new DefaultBoxAndWhiskerCategoryDataset();
		boxChart.getCategoryPlot().setDataset(0,boxDataset);
		
		//Update the histogram
		histoDataset = new PublicHistogramDataSet();
		fits = new XYSeriesCollection();
		histoChart.getXYPlot().setDataset(0,histoDataset);
		histoChart.getXYPlot().setDataset(1,fits);
		System.out.println("Number of queries in current dataset : "+newDataSet.numQueries());
		currentQuery = newDataSet.queryID(0);	
		this.histoChart.getXYPlot().getRangeAxis().setLabel("DataSet Loaded, No dimensions defined");
		this.histoChart.getXYPlot().getDomainAxis().setLabel("DataSet Loaded, No dimensions defined");
	}





	//--------------------------------------------------------------------------------------------------------------------------------
	// Listen for change of dimensions

	@Override
	public void brokerChanged(
			RedefinableBrokerContainer<DataItem, ScalarDimensionBroker<DataItem, Double>, Number> source, String query,
			ScalarDimensionBroker<DataItem, Double> old, ScalarDimensionBroker<DataItem, Double> broker) {
		
	
		if(old != null){//remove previsously loaded data
			
			//update the boxplot
			boxDataset = new DefaultBoxAndWhiskerCategoryDataset();
			boxChart.getCategoryPlot().setDataset(0,boxDataset);
			
			//Update histogram
			histoDataset = new PublicHistogramDataSet();
			histoDataset.setType(HistogramType.SCALE_AREA_TO_1);
			fits = new XYSeriesCollection();
			histoChart.getXYPlot().setDataset(0,histoDataset);
			histoChart.getXYPlot().setDataset(1,fits);
			old.removeFittingListener(this);
		}

		//load the data
		if(broker != null){

			broker.addFittingListener(this);

			//Get the selected dimensions
			dim = broker.fittableDimension(0);

			try {


				for(int q = 0; q<model.getDataSet().numQueries(); q++)			
					model.getDataSet().loadQuery(q, new HistogramDataSetAction(histoDataset, boxDataset, dim));



			} catch (DataAccessException e) {
				notif.display("Error", "An error occured while populating the 2D Scatter plot with the database content", e, Level.WARNING);
			}


			this.histoChart.getXYPlot().getDomainAxis().setLabel(dim.name());
			this.histoChart.getXYPlot().getRangeAxis().setLabel("Count");
			
			XYSplineRenderer r = (XYSplineRenderer) histoChart.getXYPlot().getRenderer();
			for(int i = 0; i<histoChart.getXYPlot().getSeriesCount(); i++){
				r.setSeriesFillPaint(i, r.getSeriesPaint(i));
			}
			
			
			//histoPanel.getChartRenderingInfo().getPlotInfo().
			
			//histoChart.getXYPlot().getRangeAxisEdge().;


		}else{
			this.histoChart.getXYPlot().getRangeAxis().setLabel("DataSet Loaded, No dimensions defined");
			this.histoChart.getXYPlot().getDomainAxis().setLabel("DataSet Loaded, No dimensions defined");
		}

	}





	@Override
	public int numDimensionalAxes() {
		return 1;
	}




	@Override
	public String axisName(int index) {
		return "Variable";
	}




	@Override
	public Dimension<DataItem,Number> currentAxisDimension(int index) {
		return dim;
	}



	
	class DistriPicker extends AbstractTotiPicker implements ChartMouseListener{



		@SuppressWarnings("unchecked")
		public void chartMouseClicked(ChartMouseEvent event)
		{
					
			ChartEntity entity = event.getEntity();      

			if (!(entity instanceof XYItemEntity))
				return;

			// Get entity details
			//String tooltip = ((XYItemEntity)entity).getToolTipText();
			XYDataset dataset = ((XYItemEntity)entity).getDataset();
			if(dataset == fits){
						
				int seriesIndex = ((XYItemEntity)entity).getSeriesIndex();
				int item = ((XYItemEntity)entity).getItem();
				XYSeries series = ((XYSeriesCollection)dataset).getSeries(seriesIndex);
				
				
				
				for(int i = 0; i<model.getDataSet().numQueries();i++){
					
					String s = model.getDataSet().queryID(i);	
					String fitId = series.getKey().toString(); 
					
					FitResult<? extends Distribution<Double>> fr = model.dimensionBroker(s).getDistributionFit(fitId);
					
					if(fr != null){			
						this.fireDistributionPicked(
								fr.getFit(),
								model.getDataSet().queryInfo(i).get(QueryInfo.TARGET), 
								(Dimension)model.dimensionBroker(s).fittableDimension(0));
					break;
					}
					
				}
				
				System.out.println("Picked fit = "+series.getKey().toString());
				
			}
		
			

		}

		public void chartMouseMoved(ChartMouseEvent event){}

	}




	public Component getView() {
		return view;
	}





	
	



}
