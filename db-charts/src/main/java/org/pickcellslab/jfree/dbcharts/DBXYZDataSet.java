package org.pickcellslab.jfree.dbcharts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.xy.DefaultXYZDataset;

@SuppressWarnings("serial")
public class DBXYZDataSet extends DefaultXYZDataset {

	private final Map<String,List<Integer>> map = new HashMap<>();
	
	public void addSeries(String key, double[][] data, List<Integer> itemIds){
		super.addSeries(key, data);
		map.put(key, itemIds);
	}
	
	
	
	public int getId(String series, int itemIndex){
		return map.get(series).get(itemIndex);
	}
}
