package org.pickcellslab.jfree.dbcharts;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;

import javax.swing.BorderFactory;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYZDataset;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.GraphSequenceConsumerFactory;
import org.pickcellslab.foundationj.dataviews.mvc.GraphSequenceViewModel;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.SeriesGeneratorBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.picking.AbstractTotiPicker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class SeriesGeneratorGraph extends AbstractTotiPicker<WritableDataItem> 
implements ReconfigurableView<
GraphSequenceViewModel<Path<NodeItem,Link>,Number>,
DataItem,
SeriesGeneratorBroker<Number>,
Number>{




	protected static final Logger log = LoggerFactory.getLogger(SeriesGeneratorGraph.class);	

	protected final NotificationFactory notif;
	
	protected JFreeChart chart;
	protected ChartPanel chartPanel;
	//protected XYLineAndShapeRenderer renderer;
	//protected XYSeriesCollection dataset =  new XYSeriesCollection();
	protected TimeTableXYDataset dataset =  new TimeTableXYDataset();

	private GraphSequenceViewModel<Path<NodeItem, Link>, Number> model;

	@SuppressWarnings("unchecked")
	private final Dimension<DataItem,Number>[] dims = new Dimension[1];
	private Function<DataItem,Number> nodeColour;
	public static final String[] axes = {"X Axis"};

	private String currentQuery;

	private double bubbleScale = 1.0;


	public SeriesGeneratorGraph(GraphSequenceViewModel<Path<NodeItem,Link>, Number> model, NotificationFactory notif, DBViewHelp helper) {

		this.notif = notif;
		
		//Create the default model
		this.model = model;

		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);






		//Initialise the JFreeChart
		chart = ChartFactory.createXYLineChart("Series Plot",
				"No dataset loaded yet",
				"No dataset loaded yet",
				dataset,
				PlotOrientation.VERTICAL,
				true,true,true
				);


		
		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseZoomable(true);
		
		chartPanel.setPreferredSize(new java.awt.Dimension(250, 250));
		chartPanel.addChartMouseListener(new DataPointPicker());

		//chartPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY,2));


		// Setup of the renderer   

		//renderer = (XYLineAndShapeRenderer) chart.getXYPlot().getRenderer();    

		//renderer.setBaseToolTipGenerator(new DataPointLabelGenerator());
		chartPanel.setBackground(Color.BLACK);
		chart.setBackgroundPaint(Color.BLACK);		
		chart.getTitle().setPaint(Color.WHITE);
		chart.getXYPlot().getDomainAxis().setTickLabelPaint(Color.WHITE);
		chart.getXYPlot().getRangeAxis().setTickLabelPaint(Color.WHITE);
		chart.getXYPlot().getDomainAxis().setLabelPaint(Color.WHITE);
		chart.getXYPlot().getRangeAxis().setLabelPaint(Color.WHITE);
		
		chart.getXYPlot().setBackgroundPaint(Color.BLACK); 
		chart.getXYPlot().setDomainGridlinePaint(Color.GRAY);
		
		chart.getXYPlot().setDrawingSupplier(
				new DefaultDrawingSupplier(
						DefaultDrawingSupplier.DEFAULT_PAINT_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_FILL_PAINT_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_PAINT_SEQUENCE,
						//DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
						customShapes()
						)
				);


		helper.registerSingleConsumersFor(this);
		helper.registerDistributionConsumerFor(this);

	}





	@Override
	public void setModel(GraphSequenceViewModel<Path<NodeItem, Link>, Number> model) {
		if(model!=null){
			model.removeDataEvtListener(this);			
		}		
		this.model = model;
		model.addBrokerChangedListener(this);
	}





	@Override
	public GraphSequenceViewModel<Path<NodeItem, Link>, Number> getModel() {
		return model;
	}


	@Override
	public String getQueryUnderFocus() {
		return currentQuery;
	}





	//--------------------------------------------------------------------------------------------------------------
	// Series appearance and visibility





	public double getBubbleScale() {
		return bubbleScale;
	}

	public void setBubbleScale(double newScale){
		bubbleScale = newScale;
		load();
	}









	//--------------------------------------------------------------------------------------------------------------------------------
	// Listen for changes to the dataset

	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {		
		dataset = new TimeTableXYDataset();	
		nodeColour = i->1;
		this.chart.getXYPlot().setDataset(0,dataset);
		this.chart.getXYPlot().setDataset(1,null);
		this.chart.clearSubtitles();
		System.out.println("Number of queries in current dataset : "+newDataSet.numQueries());
		currentQuery = newDataSet.queryID(0);	
		this.chart.getXYPlot().getRangeAxis().setLabel("DataSet Loaded, No dimensions defined");
		this.chart.getXYPlot().getDomainAxis().setLabel(model.getGeneratedDimension(0).name());
	}





	//--------------------------------------------------------------------------------------------------------------------------------
	// Listen for change of dimensions






	@Override
	public void brokerChanged(RedefinableBrokerContainer<DataItem, SeriesGeneratorBroker<Number>, Number> source,
			String query, SeriesGeneratorBroker<Number> old, SeriesGeneratorBroker<Number> broker) {

		if(old != null){//remove previsously loaded data
			dataset = new TimeTableXYDataset();		
		}

		//load the data	
		if(broker != null){
			load();

		}else{
			this.chart.getXYPlot().getRangeAxis().setLabel("DataSet Loaded, No dimensions defined");
			this.chart.getXYPlot().getDomainAxis().setLabel(model.getGeneratedDimension(0).name());
		}
	}






	public void setNodeColourDimension(Dimension<DataItem, Number> d) {
		nodeColour = d;
		load();
	}


	
	
	
	private void load(){

		final Dimension<DataItem,Number> d2 = model.dimensionBroker(this.currentQuery).dimension(0);

		dims[0] = d2;

		Function<DataItem,Number> d3 = nodeColour.andThen(n-> n==null ? Double.NaN : n.doubleValue() * bubbleScale);

		try {


			for(int q = 0; q<model.getDataSet().numQueries(); q++)			
				model.consumeQuery(q, new PathConsumerFactory(dataset, model.getGeneratedDimension(q), d2));



		} catch (DataAccessException e) {
			notif.display("Error", "An error occured while populating the 2D Scatter plot with the database content", e, Level.WARNING);
		}


	//	this.chart.getXYPlot().getDomainAxis().setLabel(d1.name());
		this.chart.getXYPlot().getRangeAxis().setLabel(d2.name());

		this.chart.getXYPlot().setDataset(dataset);
	}





	@Override
	public int numDimensionalAxes() {
		return 1;
	}




	@Override
	public String axisName(int index) {
		return axes[index];
	}




	@Override
	public Dimension<DataItem, Number> currentAxisDimension(int index) {
		return dims[index];
	}






















	//------------------------------------------------------------------------------------------------------------------------------------------













	/**
	 * Creates an array of standard shapes to display for the items in series
	 * on charts.
	 *
	 * @return The array of shapes.
	 */

	private static Shape[] customShapes() {

		Shape[] result = new Shape[10];

		double size = 2.0;
		double delta = size / 2.0;
		int[] xpoints;
		int[] ypoints;

		// square
		result[0] = new Rectangle2D.Double(-delta, -delta, size, size);
		// circle
		result[1] = new Ellipse2D.Double(-delta, -delta, size, size);

		// up-pointing triangle
		xpoints = intArray(0.0, delta, -delta);
		ypoints = intArray(-delta, delta, delta);
		result[2] = new Polygon(xpoints, ypoints, 3);

		// diamond
		xpoints = intArray(0.0, delta, 0.0, -delta);
		ypoints = intArray(-delta, 0.0, delta, 0.0);
		result[3] = new Polygon(xpoints, ypoints, 4);

		// horizontal rectangle
		result[4] = new Rectangle2D.Double(-delta, -delta / 2, size, size / 2);

		// down-pointing triangle
		xpoints = intArray(-delta, +delta, 0.0);
		ypoints = intArray(-delta, -delta, delta);
		result[5] = new Polygon(xpoints, ypoints, 3);

		// horizontal ellipse
		result[6] = new Ellipse2D.Double(-delta, -delta / 2, size, size / 2);

		// right-pointing triangle
		xpoints = intArray(-delta, delta, -delta);
		ypoints = intArray(-delta, 0.0, delta);
		result[7] = new Polygon(xpoints, ypoints, 3);

		// vertical rectangle
		result[8] = new Rectangle2D.Double(-delta / 2, -delta, size / 2, size);

		// left-pointing triangle
		xpoints = intArray(-delta, delta, delta);
		ypoints = intArray(0.0, -delta, +delta);
		result[9] = new Polygon(xpoints, ypoints, 3);

		return result;

	}



	/**
	 * Helper method to avoid lots of explicit casts in getShape().  Returns
	 * an array containing the provided doubles cast to ints.
	 *
	 * @param a  x
	 * @param b  y
	 * @param c  z
	 *
	 * @return int[3] with converted params.
	 */
	private static int[] intArray(double a, double b, double c) {
		return new int[] {(int) a, (int) b, (int) c};
	}


	/**
	 * Helper method to avoid lots of explicit casts in getShape().  Returns
	 * an array containing the provided doubles cast to ints.
	 *
	 * @param a  x
	 * @param b  y
	 * @param c  z
	 * @param d  t
	 *
	 * @return int[4] with converted params.
	 */
	private static int[] intArray(double a, double b, double c, double d) {
		return new int[] {(int) a, (int) b, (int) c, (int) d};
	}




	class DataPointPicker implements ChartMouseListener{



		public void chartMouseClicked(ChartMouseEvent event)
		{

			ChartEntity entity = event.getEntity();      

			if (!(entity instanceof XYItemEntity))
				return;

			// Get entity details
			//String tooltip = ((XYItemEntity)entity).getToolTipText();
			XYDataset dataset = ((XYItemEntity)entity).getDataset();
			int seriesIndex = ((XYItemEntity)entity).getSeriesIndex();
			int item = ((XYItemEntity)entity).getItem();


			String key = (String) ((XYZDataset)dataset).getSeriesKey(seriesIndex);


			if(dataset instanceof DBXYZDataSet){

				int itemIndex = ((DBXYZDataSet)dataset).getId(key, item);

				SeriesGeneratorGraph.this.fireSingleDataPick(
						model.getDataSet().queryInfo(0).get(QueryInfo.TARGET),
						itemIndex);
			}
		}

		public void chartMouseMoved(ChartMouseEvent event){}

	}




	private class PathConsumerFactory implements GraphSequenceConsumerFactory{

		private final TimeTableXYDataset dataset;
		private final Dimension<DataItem, Number> timeDim;
		private final Dimension<DataItem, Number> yDim;


		public PathConsumerFactory(TimeTableXYDataset dataset, final Dimension<DataItem,Number> timeDim, final  Dimension<DataItem,Number> yDim) {
			this.dataset = dataset;
			this.timeDim = timeDim;
			this.yDim = yDim;
		}


		@Override
		public Consumer<Path<NodeItem, Link>> createConsumer(String t, int u) {
			final String pathName = t+" "+u;
			return p->{
				final NodeItem n = p.last();
				final Number time = timeDim.apply(n);
				Number y = yDim.apply(n);
				if(y==null){
					Link l = p.lastEdge();
					y = yDim.apply(l);						
				}
				if(y!=null)
					dataset.add(new SimpleTimePeriod(time.intValue(),(time.intValue()+1)), y, pathName, false);
			};
		}

	}






}
