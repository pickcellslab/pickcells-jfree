package org.pickcellslab.jfree.dbcharts;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.function.Predicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.GraphSequenceViewModel;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;


@Module
public class SeriesGeneratorGraphFactory implements DBViewFactory{ 


	
	private final NotificationFactory notif;
	
	public SeriesGeneratorGraphFactory(NotificationFactory notif) {
		this.notif = notif;
	}
	
	
	@Override
	public String toString() {
		return "Time Series";
	}




	@Override
	public String description() {
		return "Creates a Time Series Plot";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}





	@Override
	public String name() {
		return "Time Series Plot";
	}



	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) {

		//target predicate
		final Predicate<DimensionContainer<DataItem, ?>> p = dc->dc.getRaw().dataType()==dType.NUMERIC;

		//Create the model 
		final GraphSequenceViewModel<Path<NodeItem, Link>, Number> model = new GraphSequenceViewModel<>(access, Mode.DECOMPOSED, p);

		//Create the scatter
		final SeriesGeneratorGraph scatter = new SeriesGeneratorGraph(model, notif, helper);


		QueryWizardConfig<DataItem, ?> config = QueryConfigs.newTargetedOnlyConfig("")
				.setAllowedQueryables(mq->{
					if(mq instanceof MetaClass)
						return GraphSequencePointer.class.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry()));
					else
						return false;				 
				}).build();


		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> config, scatter.getModel())
				.addChangeDimensionsButton(scatter)
				/*
				.addButton(new ImageIcon(getClass().getResource("/icons/bubbles.png")),
						"Bubble dimension", l->{
							DBViewHelp.displayDimensionChoice(scatter,"Bubble dimension").ifPresent(
									d-> scatter.setBubbleDimension(d));
						})
				 */
				.addButton(UITheme.resize(new ImageIcon(getClass().getResource("/icons/bubble_size_icon.png")), 16, 16),
						"Bubble Scale", l->{
							SpinnerNumberModel m = new SpinnerNumberModel(scatter.getBubbleScale(), 0.00001, 10000, 10);
							JSpinner s = new JSpinner(m);
							int i = JOptionPane.showConfirmDialog(null, s, "Bubble Scale", JOptionPane.OK_CANCEL_OPTION);
							if(i == JOptionPane.OK_OPTION)
								scatter.setBubbleScale(m.getNumber().doubleValue());
						})
				//.addCameleonControl(scatter, null)
				//.addDistributionFittingControl(scatter.getModel())
				.addExportDataButton(scatter)
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(scatter.chartPanel), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());


	}




	@Override
	public Icon icon() {		
		return new ImageIcon(getClass().getResource("/icons/time_series_icon.png"));
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
