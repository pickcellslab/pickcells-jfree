package org.pickcellslab.jfree.dbcharts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public class XYZDataSetAction implements Action<DataItem, Void> {


	private final DBXYZDataSet dataset;
	private final Function<DataItem, ? extends Number> xF;
	private final Function<DataItem, ? extends Number> yF;
	private final Function<DataItem, ? extends Number> zF;

	private final Map<Object, XYZSeriesMechanism> coords = new HashMap<>();


	public XYZDataSetAction(DBXYZDataSet dataset, Function<DataItem, ? extends Number> f1, Function<DataItem, ? extends Number> f2, Function<DataItem, ? extends Number> f3) {
		Objects.requireNonNull(dataset, "dataset is null");
		Objects.requireNonNull(f1, "f1 is null");
		Objects.requireNonNull(f2, "f2 is null");
		Objects.requireNonNull(f3, "f3 is null");

		this.dataset = dataset;
		this.xF = f1;
		this.yF = f2;
		this.zF = f3;
	}



	@Override
	public ActionMechanism<DataItem> createCoordinate(Object key) {
		System.out.println("XYZDataSetAcion : Series created "+key.toString());
		XYZSeriesMechanism m = new XYZSeriesMechanism();
		coords.put(key.toString(), m);
		return m;
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {
		XYZSeriesMechanism m = coords.get(key.toString());
		if(null!=m){
			double[][] data = new double[3][m.dataX.size()];
			for(int i = 0; i<m.dataX.size(); i++){
				data[0][i] = m.dataX.get(i).doubleValue();
				data[1][i] = m.dataY.get(i).doubleValue();
				data[2][i] = m.dataZ.get(i).doubleValue();
			}		
			dataset.addSeries(key.toString(), data, m.ids);	
		}
		m = null;
		return null;
	}

	@Override
	public String description() {
		return "Populate 2D XY Plot";
	}



	class XYZSeriesMechanism implements ActionMechanism<DataItem>{

		private final List<Number> dataX = new ArrayList<>();
		private final List<Number> dataY = new ArrayList<>();
		private final List<Number> dataZ = new ArrayList<>();
		private final List<Integer> ids = new ArrayList<>();

		@Override
		public void performAction(DataItem i) throws DataAccessException {
			Number x = xF.apply(i);
			if(null!=x){
				Number y = yF.apply(i);
				if(null!=y){
					Number z = zF.apply(i);
					if(null!=z){
						dataX.add(x);dataY.add(y); dataZ.add(z);
						ids.add(i.getAttribute(DataItem.idKey).get());
					}
				}
			}
		}

	}




}
