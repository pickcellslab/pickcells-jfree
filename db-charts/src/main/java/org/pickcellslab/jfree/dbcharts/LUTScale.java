package org.pickcellslab.jfree.dbcharts;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.renderer.PaintScale;
import org.pickcellslab.pickcells.api.app.charts.RGBLut;

public class LUTScale implements PaintScale {


	private final byte[][] lut;
	private final double lowerBound;
	private final double upperBound;

	public LUTScale(double lowerBound, double upperBound, RGBLut lut) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.lut = lut.data();
	}


	@Override
	public double getLowerBound() {
		return lowerBound;
	}

	@Override
	public double getUpperBound() {
		return upperBound;
	}

	@Override
	public Paint getPaint(double value) {

		double v = Math.max(value, this.lowerBound);
		v = Math.min(v, this.upperBound);
		int i = (int) ((v - this.lowerBound) / (this.upperBound
				- this.lowerBound) * 255.0);

		if(i == 12)
			return Color.BLACK;
		
		return new Color(Byte.toUnsignedInt(lut[0][i]), Byte.toUnsignedInt(lut[1][i]), Byte.toUnsignedInt(lut[2][i]));
	}

}
