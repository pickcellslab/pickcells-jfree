package org.pickcellslab.jfree.dbcharts;

import java.awt.Color;
import java.awt.Component;
import java.awt.Paint;
import java.awt.RadialGradientPaint;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.MultiplePiePlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.util.TableOrder;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dataviews.mvc.CameleonView;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dataviews.mvc.DefaultCategoricalModel;

public class PieChart implements DataView<DefaultCategoricalModel, DataItem>, CameleonView<Color> {


	private static final Point2D center = new Point2D.Float(0, 0);
	private static final float radius = 200;
	private static final float[] dist = {0.2f, 1.0f};


	private static final RadialGradientPaint[] paints = {
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.BLUE, new Color(200, 200, 255)}),
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.RED, new Color(255, 200, 200)}),
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.YELLOW, new Color(255, 255, 200)}),
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.GREEN, new Color(200, 255, 200)}),
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.ORANGE, new Color(255, 200, 200)}),
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.DARK_GRAY, new Color(200, 200, 200)}),
			new RadialGradientPaint(center, radius, dist,
					new Color[] {Color.MAGENTA, new Color(255, 200, 255)})
	};



	private DefaultCategoricalModel model;
	private DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	private ChartPanel chartPanel;
	private final JFreeChart chart;



	public PieChart(DefaultCategoricalModel model) {

		this.model = model;
		model.addDataEvtListener(this);

		// Create the Chart
		chart = ChartFactory.createMultiplePieChart(
				"No DataSet",
				dataset,
				TableOrder.BY_ROW,
				false, // legend?
				true, // tooltips?
				false // URLs?
				);

		MultiplePiePlot plot = (MultiplePiePlot) chart.getPlot();
		PiePlot pplot = (PiePlot) plot.getPieChart().getPlot();
		pplot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({2})"));
		pplot.setBackgroundPaint(Color.WHITE);
		pplot.setOutlinePaint(null);
		
		pplot.setBaseSectionPaint(paints[0]);
		
		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseZoomable(true);
		chartPanel.setMouseWheelEnabled(true);
		chartPanel.setBackground(Color.LIGHT_GRAY);
		chartPanel.setPreferredSize(new java.awt.Dimension(450, 450));
		chartPanel.addChartMouseListener(new SectionPicker());

		chartPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY,2));

	}


	public Component getView(){
		return chartPanel;
	}






	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {

		dataset.clear();
		
		MultiplePiePlot plot = (MultiplePiePlot)chart.getPlot();
		PiePlot pplot = (PiePlot) plot.getPieChart().getPlot();
		

		for(int q = 0; q<newDataSet.numQueries(); q++){
			String queryID = newDataSet.queryID(q);
			int total = model.count(queryID);
			//System.out.println("MultiPiePlot --> Total in Query : "+queryID+" = "+total);
			for(int s = 0; s<newDataSet.numSeries(q); s++){
				String seriesID = newDataSet.seriesID(q, s);
				dataset.addValue(model.ids(queryID, seriesID).size(), queryID, seriesID);
				//System.out.println("MultiPiePlot --> Added : "+queryID+" / "+seriesID+" : "+model.ids(queryID, seriesID).size());
				pplot.setSectionPaint(seriesID, getNextPaint());
			}
		}

		if(dataset.getRowCount() == 0)
			chart.setTitle("No Data");
		else
			chart.setTitle("");

		
		plot.setDataset(dataset);

		
		
	}


	private int n = 0;

	private Paint getNextPaint() {
		return paints[n++ % paints.length];
	}

	
	

	@Override
	public void setModel(DefaultCategoricalModel model) {		
		if(this.model!=null)
			this.model.removeDataEvtListener(this);
		this.model = model;
		model.addDataEvtListener(this);
		this.dataSetChanged(null, model.getDataSet());
	}



	@Override
	public DefaultCategoricalModel getModel() {
		return model;
	}




	@Override
	public void setSeriesVisible(String query, int series, boolean visible) {
		// TODO Auto-generated method stub

	}


	@Override
	public Color getSeriesAppearance(String query, int series) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setSeriesAppearance(String query, int series, Color appearance) {
		// TODO Auto-generated method stub

	}

}
