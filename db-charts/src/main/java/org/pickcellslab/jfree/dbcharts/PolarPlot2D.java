package org.pickcellslab.jfree.dbcharts;

import java.awt.Color;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.FastMath;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PolarPlot;
import org.jfree.chart.renderer.DefaultPolarItemRenderer;
import org.jfree.data.statistics.HistogramType;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.TransformableDimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.VectorDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.DirectionalDBChart;
import org.pickcellslab.pickcells.api.app.charts.VectorChartModel;
import org.pickcellslab.pickcells.api.app.charts.VectorRotationFactory;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistributionFitterFactory;
import org.pickcellslab.pickcells.api.app.picking.AbstractTotiPicker;

public class PolarPlot2D extends AbstractTotiPicker<DataItem> implements DirectionalDBChart<SeriesAppearance>{


	private final NotificationFactory notif;
	
	private VectorChartModel<DataItem> model;
	private JFreeChart chart;
	ChartPanel chartPanel;
	private PublicHistogramDataSet histoDataset = new PublicHistogramDataSet();
	private Dimension<DataItem, double[]> dim;
	private String currentQuery;	

	public PolarPlot2D(NotificationFactory notif, DBViewHelp helper, List<DirectionalRealMultiVariateDistributionFitterFactory> fitters) {

		
		this.notif = notif;
		
		//Create the default model 
		final Predicate<Integer> p = (i)-> i==2 || i == 3;
		model = new VectorChartModel(p, fitters, new VectorRotationFactory());

		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);


		//Initialise the JFreeChart
		chart = ChartFactory.createPolarChart(
				"2D Polar Chart",
				histoDataset,
				true,true,true
				);



		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseZoomable(true);
		chartPanel.setPreferredSize(new java.awt.Dimension(250, 250));
		//TODO chartPanel.addChartMouseListener(new DataPointPicker());

		//chartPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY,2));


		// Setup of the renderer   

		DefaultPolarItemRenderer r = (DefaultPolarItemRenderer) ((PolarPlot)chart.getPlot()).getRenderer();
		r.setShapesVisible(false);
		r.setSeriesFilled(0, true);

		//renderer = (XYLineAndShapeRenderer) chart.getXYPlot().getRenderer();    

		//renderer.setBaseToolTipGenerator(new DataPointLabelGenerator());

		//chart.getPlot().setBackgroundPaint(Color.WHITE); 
		chartPanel.setBackground(Color.BLACK);
		chart.setBackgroundPaint(Color.BLACK);		
		chart.getTitle().setPaint(Color.WHITE);
		chart.getLegend().setBackgroundPaint(Color.BLACK);
		chart.getLegend().setItemPaint(Color.WHITE);
		chart.getPlot().setBackgroundPaint(Color.BLACK); 
		((PolarPlot) chart.getPlot()).getAxis().setLabelPaint(Color.WHITE);
		((PolarPlot) chart.getPlot()).setAngleLabelPaint(Color.WHITE);
		((PolarPlot) chart.getPlot()).setRadiusMinorGridlinesVisible(false);
		((PolarPlot) chart.getPlot()).setAngleTickUnit(new NumberTickUnit(90));

		helper.registerSingleConsumersFor(this);
		helper.registerDistributionConsumerFor(this);

	}




	@Override
	public void setSeriesVisible(String query, int series, boolean visible) {
		// TODO Auto-generated method stub

	}

	@Override
	public SeriesAppearance getSeriesAppearance(String query, int series) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSeriesAppearance(String query, int series, SeriesAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public SeriesAppearance getFitAppearance(FitResult<?> fit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFitAppearance(FitResult<?> fit, SeriesAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFitVisible(FitResult<?> fit, boolean isVisible) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fittingUpdated(FittingEvent evt) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getQueryUnderFocus() {
		return currentQuery;
	}

	@Override
	public int numDimensionalAxes() {
		return 1;
	}

	@Override
	public String axisName(int index) {
		return "Angle";
	}

	@Override
	public Dimension<DataItem, ?> currentAxisDimension(int index) {
		
		if(dim==null)
			return null;
		
		final Dimension<DataItem, double[]> fDim = dim;
		
		return new Dimension<DataItem, Double>(){

			@Override
			public dType dataType() {
				return dType.NUMERIC;
			}

			@Override
			public String description() {
				return fDim.description();
			}

			@Override
			public Class<Double> getReturnType() {
				return Double.class;
			}

			@Override
			public Double apply(DataItem t) {
				double[] n = fDim.apply(t);
				if(null!=n && !Double.isNaN(n[0])){
					final double z = n.length == 2 ? 0 : n[2];
					Vector3D v = new Vector3D(n[0],n[1],z);
					return FastMath.toDegrees(Vector3D.angle(v, Vector3D.PLUS_K));
				}
				else return Double.NaN;
			}

			@Override
			public int index() {
				return -1;
			}

			@Override
			public int length() {
				return -1;
			}

			@Override
			public String name() {
				return fDim.name();
			}

			@Override
			public String info() {
				return fDim.info();
			}
			
		};
	}

	@Override
	public void setModel(VectorChartModel<DataItem> model) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public VectorChartModel<DataItem> getModel() {
		return model;
	}

	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {
		// TODO Auto-generated method stub
		currentQuery = newDataSet.queryID(0);
		histoDataset = new PublicHistogramDataSet();
		histoDataset.setType(HistogramType.SCALE_AREA_TO_1);
		((PolarPlot)chart.getPlot()).setDataset(0,histoDataset);
	}

	@Override
	public void brokerChanged(RedefinableBrokerContainer<DataItem, VectorDimensionBroker<DataItem>, double[]> source,
			String query, VectorDimensionBroker<DataItem> old, VectorDimensionBroker<DataItem> broker) {

		if(old != null){//remove previsously loaded data
			//Update histogram
			histoDataset = new PublicHistogramDataSet();
			histoDataset.setType(HistogramType.SCALE_AREA_TO_1);
			//	fits = new XYSeriesCollection();
			((PolarPlot)chart.getPlot()).setDataset(0,histoDataset);
			//	histoChart.getXYPlot().setDataset(1,fits);
			old.removeFittingListener(this);
			old.removeTransformChangeListener(this);
		}

		//load the data
		if(broker != null){

			broker.addFittingListener(this);
			broker.addTransformChangeListener(this);

			//Get the selected dimensions
			dim = broker.fittableDimension(0);

			try {


				for(int q = 0; q<model.getDataSet().numQueries(); q++)			
					model.getDataSet().loadQuery(q, new PolarPlotAction(histoDataset, dim));



			} catch (DataAccessException e) {
				notif.display("Error", "An error occured while populating the Polar plot with the database content", e, Level.WARNING);
			}



			//histoPanel.getChartRenderingInfo().getPlotInfo().

			//histoChart.getXYPlot().getRangeAxisEdge();


		}



	}

	@Override
	public void transformChanged(TransformableDimensionBroker<DataItem, double[]> source) {
		
		dim = source.dimension(0);
		
		try {
			
			histoDataset = new PublicHistogramDataSet();
			histoDataset.setType(HistogramType.SCALE_AREA_TO_1);
			//	fits = new XYSeriesCollection();
			((PolarPlot)chart.getPlot()).setDataset(0,histoDataset);

			for(int q = 0; q<model.getDataSet().numQueries(); q++)			
				model.getDataSet().loadQuery(q, new PolarPlotAction(histoDataset, dim));


		} catch (DataAccessException e) {
			notif.display("Error", "An error occured while populating the Polar plot with the database content", e, Level.WARNING);
		}
		
	}

}
