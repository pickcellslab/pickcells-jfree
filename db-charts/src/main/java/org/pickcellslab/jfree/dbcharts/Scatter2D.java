package org.pickcellslab.jfree.dbcharts;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;

import javax.swing.BorderFactory;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYBlockRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYZDataset;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.ScalarDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.NumericChartModel;
import org.pickcellslab.pickcells.api.app.charts.StandardNumericDBChart;
import org.pickcellslab.pickcells.api.app.picking.AbstractTotiPicker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class Scatter2D extends AbstractTotiPicker<DataItem> implements StandardNumericDBChart<double[],SeriesAppearance>{




	protected static final Logger log = LoggerFactory.getLogger(Scatter2D.class);	

	protected final NotificationFactory notif;
	
	protected JFreeChart chart;
	protected ChartPanel chartPanel;
	//protected XYLineAndShapeRenderer renderer;
	//protected XYSeriesCollection dataset =  new XYSeriesCollection();
	protected DBXYZDataSet dataset =  new DBXYZDataSet();

	private NumericChartModel<DataItem,double[]> model;

	@SuppressWarnings("unchecked")
	private final Dimension<DataItem,Number>[] dims = new Dimension[2];
	private Function<DataItem,Number> bubbles;
	public static final String[] axes = {"X Axis", "Y Axis"};

	private String currentQuery;

	private double bubbleScale = 1.0;


	public Scatter2D(NotificationFactory notif, DBViewHelp helper, List<MultiVariateRealDistributionFitterFactory> fitters) {

		this.notif = notif;
		
		//Create the default model
		final Predicate<Integer> p = i->i==2;
		model = new NumericChartModel<>(p, (List)fitters);

		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);






		//Initialise the JFreeChart
		chart = ChartFactory.createBubbleChart("2D Scatter Plot",//.createScatterPlot("2D Scatter Plot",
				"No dataset loaded yet",
				"No dataset loaded yet",
				(XYZDataset) dataset,
				PlotOrientation.VERTICAL,
				true,true,true
				);



		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseZoomable(true);
		chartPanel.setBackground(Color.LIGHT_GRAY);
		chartPanel.setPreferredSize(new java.awt.Dimension(250, 250));
		chartPanel.addChartMouseListener(new DataPointPicker());

		chartPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY,2));


		// Setup of the renderer   

		//renderer = (XYLineAndShapeRenderer) chart.getXYPlot().getRenderer();    

		//renderer.setBaseToolTipGenerator(new DataPointLabelGenerator());
		chartPanel.setBackground(Color.BLACK);
		chart.setBackgroundPaint(Color.BLACK);		
		chart.getTitle().setPaint(Color.WHITE);
		chart.getXYPlot().getDomainAxis().setTickLabelPaint(Color.WHITE);
		chart.getXYPlot().getRangeAxis().setTickLabelPaint(Color.WHITE);
		chart.getXYPlot().getDomainAxis().setLabelPaint(Color.WHITE);
		chart.getXYPlot().getRangeAxis().setLabelPaint(Color.WHITE);
		
		chart.getXYPlot().setBackgroundPaint(Color.BLACK); 
		chart.getXYPlot().setDomainGridlinePaint(Color.GRAY);

		chart.getXYPlot().setDrawingSupplier(
				new DefaultDrawingSupplier(
						DefaultDrawingSupplier.DEFAULT_PAINT_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_FILL_PAINT_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_PAINT_SEQUENCE,
						//DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE,
						DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
						customShapes()
						)
				);


		helper.registerSingleConsumersFor(this);
		helper.registerDistributionConsumerFor(this);

	}




	@Override
	public void setModel(NumericChartModel<DataItem,double[]> model) {
		if(model!=null){
			model.removeDataEvtListener(this);			
		}		
		this.model = model;
		model.addBrokerChangedListener(this);		
	}



	@Override
	public NumericChartModel<DataItem,double[]> getModel() {
		return model;
	}


	@Override
	public String getQueryUnderFocus() {
		return currentQuery;
	}





	//--------------------------------------------------------------------------------------------------------------
	// Series appearance and visibility

	@Override
	public void setSeriesVisible(String query, int series, boolean visible) {
		//TODO
		//String s = model.getDataSet().seriesID(model.getDataSet().queryIndex(query), series);
	}




	@Override
	public SeriesAppearance getSeriesAppearance(String query, int series) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setSeriesAppearance(String query, int series, SeriesAppearance appearance) {
		// TODO Auto-generated method stub

	}


	
	public double getBubbleScale() {
		return bubbleScale;
	}

	public void setBubbleScale(double newScale){
		bubbleScale = newScale;
		load();
	}
	
	






	//-----------------------------------------------------------------------------------------------------------------
	// Fitting




	@Override
	public void fittingUpdated(FittingEvent evt) {
		FitResult<? extends Distribution<double[]>> r = model.dimensionBroker(currentQuery).getDistributionFit(evt.stringId());
		chart.clearSubtitles();
		chart.addSubtitle(new TextTitle(r.getDescription()));
		XYDistributionDataSet fitDS = new XYDistributionDataSet(
				r.getFit(),
				chart.getXYPlot().getDomainAxis().getLowerBound(),
				chart.getXYPlot().getDomainAxis().getUpperBound(),
				chart.getXYPlot().getRangeAxis().getLowerBound(),
				chart.getXYPlot().getRangeAxis().getUpperBound());
		chart.getXYPlot().setDataset(1, fitDS);

		XYBlockRenderer renderer = new XYBlockRenderer();

		chart.getXYPlot().setRenderer(1,renderer);		
		fitDS.print();
		renderer.setPaintScale(fitDS.createStretchedScale());
	}

	@Override
	public SeriesAppearance getFitAppearance(FitResult<?> fit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFitAppearance(FitResult<?> fit, SeriesAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFitVisible(FitResult<?> fit, boolean isVisible) {
		// TODO Auto-generated method stub

	}







	//--------------------------------------------------------------------------------------------------------------------------------
	// Listen for changes to the dataset

	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {		
		dataset = new DBXYZDataSet();	
		bubbles = i->1;
		this.chart.getXYPlot().setDataset(0,dataset);
		this.chart.getXYPlot().setDataset(1,null);
		this.chart.clearSubtitles();
		System.out.println("Number of queries in current dataset : "+newDataSet.numQueries());
		currentQuery = newDataSet.queryID(0);	
		this.chart.getXYPlot().getRangeAxis().setLabel("DataSet Loaded, No dimensions defined");
		this.chart.getXYPlot().getDomainAxis().setLabel("DataSet Loaded, No dimensions defined");
	}





	//--------------------------------------------------------------------------------------------------------------------------------
	// Listen for change of dimensions
	@Override
	public void brokerChanged(
			RedefinableBrokerContainer<DataItem, ScalarDimensionBroker<DataItem, double[]>, Number> source,
			String query, ScalarDimensionBroker<DataItem, double[]> old,
			ScalarDimensionBroker<DataItem, double[]> broker) {


		if(old != null){//remove previsously loaded data
			dataset = new DBXYZDataSet();			
			old.removeFittingListener(this);
		}

		//load the data	
		if(broker != null){

			broker.addFittingListener(this);

			load();

		}else{
			this.chart.getXYPlot().getRangeAxis().setLabel("DataSet Loaded, No dimensions defined");
			this.chart.getXYPlot().getDomainAxis().setLabel("DataSet Loaded, No dimensions defined");
		}


	}


	public void setBubbleDimension(Dimension<DataItem, Number> d) {
		bubbles = d;
		load();
	}


	private void load(){

		//Get the selected dimensions
		Dimension<DataItem,Number> d1 = model.dimensionBroker(this.currentQuery).fittableDimension(0);
		Dimension<DataItem,Number> d2 = model.dimensionBroker(this.currentQuery).fittableDimension(1);

		dims[0] = d1; dims[1] = d2;

		Function<DataItem,Number> d3 = bubbles.andThen(n-> n==null ? Double.NaN : n.doubleValue() * bubbleScale);

		try {


			for(int q = 0; q<model.getDataSet().numQueries(); q++)			
				model.getDataSet().loadQuery(q, new XYZDataSetAction(dataset, d1, d2, d3));



		} catch (DataAccessException e) {
			notif.display("Error", "An error occured while populating the 2D Scatter plot with the database content", e, Level.WARNING);
		}


		this.chart.getXYPlot().getDomainAxis().setLabel(d1.name());
		this.chart.getXYPlot().getRangeAxis().setLabel(d2.name());

		this.chart.getXYPlot().setDataset(dataset);
	}





	@Override
	public int numDimensionalAxes() {
		return 2;
	}




	@Override
	public String axisName(int index) {
		return axes[index];
	}




	@Override
	public Dimension<DataItem,Number> currentAxisDimension(int index) {
		return dims[index];
	}






















	//------------------------------------------------------------------------------------------------------------------------------------------













	/**
	 * Creates an array of standard shapes to display for the items in series
	 * on charts.
	 *
	 * @return The array of shapes.
	 */

	private static Shape[] customShapes() {

		Shape[] result = new Shape[10];

		double size = 2.0;
		double delta = size / 2.0;
		int[] xpoints;
		int[] ypoints;

		// square
		result[0] = new Rectangle2D.Double(-delta, -delta, size, size);
		// circle
		result[1] = new Ellipse2D.Double(-delta, -delta, size, size);

		// up-pointing triangle
		xpoints = intArray(0.0, delta, -delta);
		ypoints = intArray(-delta, delta, delta);
		result[2] = new Polygon(xpoints, ypoints, 3);

		// diamond
		xpoints = intArray(0.0, delta, 0.0, -delta);
		ypoints = intArray(-delta, 0.0, delta, 0.0);
		result[3] = new Polygon(xpoints, ypoints, 4);

		// horizontal rectangle
		result[4] = new Rectangle2D.Double(-delta, -delta / 2, size, size / 2);

		// down-pointing triangle
		xpoints = intArray(-delta, +delta, 0.0);
		ypoints = intArray(-delta, -delta, delta);
		result[5] = new Polygon(xpoints, ypoints, 3);

		// horizontal ellipse
		result[6] = new Ellipse2D.Double(-delta, -delta / 2, size, size / 2);

		// right-pointing triangle
		xpoints = intArray(-delta, delta, -delta);
		ypoints = intArray(-delta, 0.0, delta);
		result[7] = new Polygon(xpoints, ypoints, 3);

		// vertical rectangle
		result[8] = new Rectangle2D.Double(-delta / 2, -delta, size / 2, size);

		// left-pointing triangle
		xpoints = intArray(-delta, delta, delta);
		ypoints = intArray(0.0, -delta, +delta);
		result[9] = new Polygon(xpoints, ypoints, 3);

		return result;

	}



	/**
	 * Helper method to avoid lots of explicit casts in getShape().  Returns
	 * an array containing the provided doubles cast to ints.
	 *
	 * @param a  x
	 * @param b  y
	 * @param c  z
	 *
	 * @return int[3] with converted params.
	 */
	private static int[] intArray(double a, double b, double c) {
		return new int[] {(int) a, (int) b, (int) c};
	}


	/**
	 * Helper method to avoid lots of explicit casts in getShape().  Returns
	 * an array containing the provided doubles cast to ints.
	 *
	 * @param a  x
	 * @param b  y
	 * @param c  z
	 * @param d  t
	 *
	 * @return int[4] with converted params.
	 */
	private static int[] intArray(double a, double b, double c, double d) {
		return new int[] {(int) a, (int) b, (int) c, (int) d};
	}




	class DataPointPicker implements ChartMouseListener{



		public void chartMouseClicked(ChartMouseEvent event)
		{

			ChartEntity entity = event.getEntity();      

			if (!(entity instanceof XYItemEntity))
				return;

			// Get entity details
			//String tooltip = ((XYItemEntity)entity).getToolTipText();
			XYDataset dataset = ((XYItemEntity)entity).getDataset();
			int seriesIndex = ((XYItemEntity)entity).getSeriesIndex();
			int item = ((XYItemEntity)entity).getItem();


			String key = (String) ((XYZDataset)dataset).getSeriesKey(seriesIndex);


			if(dataset instanceof DBXYZDataSet){
				
				int itemIndex = ((DBXYZDataSet)dataset).getId(key, item);

				Scatter2D.this.fireSingleDataPick(
						model.getDataSet().queryInfo(0).get(QueryInfo.TARGET),
						itemIndex);
			}
			else{
				
				
				Dimension<DataItem,Number> d1 = model.dimensionBroker(Scatter2D.this.currentQuery).fittableDimension(0);
				Dimension<DataItem,Number> d2 = model.dimensionBroker(Scatter2D.this.currentQuery).fittableDimension(1);
				Function<DataItem,double[]> f = i->{
					Number x = d1.apply(i);
					if(null!=x){
						Number y = d2.apply(i);
						if(null!=y){
							return new double[]{x.doubleValue(),y.doubleValue()};
						}
					}
					return new double[]{Double.NaN, Double.NaN};
				};
				
				Scatter2D.this.fireDistributionPicked(
						((XYDistributionDataSet) dataset).getDistribution(),
						model.getDataSet().queryInfo(0).get(QueryInfo.TARGET),
						f);
			}
			
			
			
		}

		public void chartMouseMoved(ChartMouseEvent event){}

	}




	



















}
