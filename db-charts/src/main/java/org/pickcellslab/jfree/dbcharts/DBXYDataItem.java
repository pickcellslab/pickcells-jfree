package org.pickcellslab.jfree.dbcharts;

import org.jfree.data.xy.XYDataItem;

@SuppressWarnings("serial")
public class DBXYDataItem extends XYDataItem{

	private int id;

	public DBXYDataItem(Number x, Number y, int id) {
		super(x, y);
		this.id = id;
	}

	public int getId(){
		return id;
	}
	
	
	
}
