package org.pickcellslab.jfree.dbcharts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public class PolarPlotAction implements Action<DataItem,Void> {

	private final Dimension<DataItem, double[]> dim;

	private final Map<Object, List<Double>> coords = new HashMap<>();

	private int bins = -1;

	private final PublicHistogramDataSet dataset;


	public PolarPlotAction(PublicHistogramDataSet dataset, Dimension<DataItem, double[]> dim) {
		this.dim = dim;
		this.dataset = dataset;
	}


	@Override
	public ActionMechanism<DataItem> createCoordinate(Object key) {
		List<Double> l = new ArrayList<>();
		coords.put(key, l);
		if(dim.length() == 2)
			return new PolarMechanism2D(l);
		else
			return new PolarMechanism3D(l);
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {

		if(bins == -1){
			int largest = 0;
			for(List<Double> l : coords.values()){
				if(l.size()>largest)
					largest = l.size();
			}
			bins = (int) Math.ceil(Math.sqrt(largest));
		}



		List<Double> l = coords.get(key);
		if(l == null)	return null;
		if(l.isEmpty()) return null;

		double[] data = new double[l.size()];
		for(int i = 0; i<l.size(); i++)
			data[i] = l.get(i);		

		dataset.addSeries(key.toString(), data, bins);


		return null;
	}

	@Override
	public String description() {
		return "Populating JFreeChart Polar Plot with " + dim.name();
	}


	private class PolarMechanism2D implements ActionMechanism<DataItem>{

		private final Vector2D ref = new Vector2D(0,1);
		private final List<Double> list;

		public PolarMechanism2D(List<Double> list) {
			this.list = list;
		}

		@Override
		public void performAction(DataItem i) throws DataAccessException {
			//Check for null
			double[] n = dim.apply(i);
			if(null!=n && !Double.isNaN(n[0])){
				Vector2D v = new Vector2D(n[0],n[1]);
				list.add(FastMath.toDegrees(Vector2D.angle(v, ref)));
			}
		}

	}

	private class PolarMechanism3D implements ActionMechanism<DataItem>{

		private final Vector3D ref = new Vector3D(0,0,1);
		private final List<Double> list;

		public PolarMechanism3D(List<Double> list) {
			this.list = list;
		}

		@Override
		public void performAction(DataItem i) throws DataAccessException {
			//Check for null
			double[] n = dim.apply(i);
			if(null!=n && !Double.isNaN(n[0])){
				Vector3D v = new Vector3D(n[0],n[1],n[2]);
				list.add(FastMath.toDegrees(Vector3D.angle(v, ref)));
			}
		}

	}




}
