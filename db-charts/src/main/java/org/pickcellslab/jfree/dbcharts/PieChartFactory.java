package org.pickcellslab.jfree.dbcharts;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.mvc.DefaultCategoricalModel;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;

@Module
public class PieChartFactory implements DBViewFactory{ 

	private final UITheme theme;
	
	public PieChartFactory(UITheme theme) {
		this.theme = theme;
	}
	
	
	@Override
	public String toString() {
		return "Pie Chart";
	}




	@Override
	public String description() {
		return "Create a Pie Chart";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}







	@Override
	public String name() {
		return "Pie Chart";
	}




	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) {
		
		//Create the scatter
		PieChart pie = new PieChart(new DefaultCategoricalModel(null));

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> QueryConfigs.newTargetedOnlyConfig("").setSynchronizeTarget(true).build(), pie.getModel())
				//.addCameleonControl(scatter, null)
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(pie.getView()), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());


	}




	@Override
	public Icon icon() {
		return theme.icon(IconID.Charts.CHART_PIE, 24);
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
