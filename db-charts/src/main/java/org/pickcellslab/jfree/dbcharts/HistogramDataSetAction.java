package org.pickcellslab.jfree.dbcharts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.jfree.data.statistics.BoxAndWhiskerItem;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public class HistogramDataSetAction implements Action<DataItem, Void> {

	private final PublicHistogramDataSet histoDataset;
	private DefaultBoxAndWhiskerCategoryDataset boxDataset;
	private final Dimension<DataItem, ? extends Number> dim;
	
	private final Map<Object, List<Double>> coords = new HashMap<>();
	
	private int bins = -1;
	

	public HistogramDataSetAction(PublicHistogramDataSet dataset, DefaultBoxAndWhiskerCategoryDataset dataset2, Dimension<DataItem,? extends Number> dim) {
		this.histoDataset = dataset;
		this.boxDataset = dataset2;
		this.dim = dim;
	}


	@Override
	public ActionMechanism<DataItem> createCoordinate(Object key) {
		List<Double> l = new ArrayList<>();
		coords.put(key, l);
		return new HistogramMechanism(l);
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {
		
		if(bins == -1){
			int largest = 0;
			for(List<Double> l : coords.values()){
				if(l.size()>largest)
					largest = l.size();
			}
			bins = (int) Math.ceil(Math.sqrt(largest));
		}
		
		
		
		final List<Double> l = coords.get(key);	
		if(l == null)	return null;
		if(l.isEmpty()) return null;
		
		Collections.sort(l);//Sort for percentile quantification
		final DescriptiveStatistics stats = new DescriptiveStatistics();
		final double[] data = new double[l.size()];
		for(int i = 0; i<l.size(); i++){
			data[i] = l.get(i);		
			stats.addValue(l.get(i));
		}
		histoDataset.addSeries(key.toString(), data, bins);
		
		//boxDataset.add(l, key.toString(), dim.name());
		
		final double iqr = stats.getPercentile(75) - stats.getPercentile(25);
		final double minOut = stats.getPercentile(25) - 1.5*iqr;
		final double maxOut = stats.getPercentile(75) + 1.5*iqr;
		List<Double> outliers = l.stream().filter(p-> p<minOut || p>maxOut).collect(Collectors.toList());
		
		boxDataset.add(new BoxAndWhiskerItem(
				stats.getMean(),
				stats.getPercentile(50),
				stats.getPercentile(25),
				stats.getPercentile(75),
				stats.getMin(),
				stats.getMax(),
				minOut,
				maxOut,
				outliers), key.toString(), dim.name());
		
		//System.out.println("HistogramAction: Added -> "+key.toString()+" ; "+dim.name());
	//	System.out.println("HistogramAction: List size -> "+l.size()+" ; Array Size "+data.length);
		
		
		//l.forEach(v->stats.addValue(v));
		//System.out.println("HistogramAction: -> "+stats.toString());
		
		return null;
	}

	@Override
	public String description() {
		return "Populating JFreeChart histogram with " + dim.name();
	}


	private class HistogramMechanism implements ActionMechanism<DataItem>{

		
		private final List<Double> list;

		public HistogramMechanism(List<Double> list) {
			this.list = list;
		}
		
		@Override
		public void performAction(DataItem i) throws DataAccessException {
			//Check for null
			Number n = dim.apply(i);
			if(null!=n)
				list.add(n.doubleValue());
		}
		
	}
	

}
