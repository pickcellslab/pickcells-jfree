package org.pickcellslab.jfree.dbcharts;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistributionFitterFactory;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;


@Module
public class Scatter2DFactory implements DBViewFactory{ 


	private final NotificationFactory notif;
	private final List<MultiVariateRealDistributionFitterFactory> fitters;
	



	public Scatter2DFactory(NotificationFactory notif, List<MultiVariateRealDistributionFitterFactory> fitters) {
		this.notif = notif;
		this.fitters = fitters;
	}
	
	
	
	@Override
	public String toString() {
		return "Bubble Plot";
	}




	@Override
	public String description() {
		return "Creates a 2D Bubble Plot";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}







	@Override
	public String name() {
		return "Bubble Plot 2D";
	}




	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) {

		//Create the scatter
		Scatter2D scatter = new Scatter2D(notif, helper, fitters);

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> QueryConfigs.newTargetedOnlyConfig("").setSynchronizeTarget(true).build(), scatter.getModel())
				.addChangeDimensionsButton(scatter)
				.addButton(UITheme.resize(new ImageIcon(getClass().getResource("/icons/bubble_dim_icon.png")), 16, 16),
						"Bubble dimension", l->{
							helper.displayDimensionChoice(scatter,"Bubble Dimension").ifPresent(
									d-> scatter.setBubbleDimension(d));
						})
				.addButton(UITheme.resize(new ImageIcon(getClass().getResource("/icons/bubble_size_icon.png")), 16, 16),
						"Bubble Scale", l->{
							SpinnerNumberModel m = new SpinnerNumberModel(scatter.getBubbleScale(), 0.00001, 10000, 10);
							JSpinner s = new JSpinner(m);
							int i = JOptionPane.showConfirmDialog(null, s, "Bubble Scale", JOptionPane.OK_CANCEL_OPTION);
							if(i == JOptionPane.OK_OPTION)
								scatter.setBubbleScale(m.getNumber().doubleValue());
						})
				.addCameleonControl(scatter, null)
				.addDistributionFittingControl(scatter.getModel())
				.addExportDataButton(scatter)
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(scatter.chartPanel), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());


	}




	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/bubble_plot_icon.png"));
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}



}
