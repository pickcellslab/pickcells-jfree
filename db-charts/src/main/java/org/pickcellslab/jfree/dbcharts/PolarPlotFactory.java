package org.pickcellslab.jfree.dbcharts;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistributionFitterFactory;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;


@Module
public class PolarPlotFactory implements DBViewFactory {

	private final NotificationFactory notif;
	private final List<DirectionalRealMultiVariateDistributionFitterFactory> fitters;
	
	public PolarPlotFactory(NotificationFactory notif, List<DirectionalRealMultiVariateDistributionFitterFactory> fitters) {
		this.notif = notif;
		this.fitters = fitters;
	}
	
	
	@Override
	public String name() {
		return "Rose Diagram";
	}

	@Override
	public String description() {
		return "<HTML><b>Rose Diagram</b>"
				+ "<br> Create a Rose Diagram to display the distribution of angualr data</br></HTML>";
	}

	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}

	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) throws ViewFactoryException {

		//Create the scatter
		PolarPlot2D scatter = new PolarPlot2D(notif, helper, fitters);

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> QueryConfigs.newTargetedOnlyConfig("").setSynchronizeTarget(true).build(), scatter.getModel())
				.addChangeDimensionsButton(scatter)
				.addCameleonControl(scatter, null)
				.addTransformDimensionControl(scatter, "Normalize Vectors to other Vectors")
				.addDistributionFittingControl(scatter.getModel())
				.addExportDataButton(scatter)						
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(scatter.chartPanel), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());



}

@Override
public Icon icon() {
	return new ImageIcon(getClass().getResource("/icons/rose_icon.png"));
}

@Override
public String authors() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public String licence() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public URL url() {
	// TODO Auto-generated method stub
	return null;
}

}
